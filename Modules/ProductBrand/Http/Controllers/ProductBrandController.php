<?php

namespace Modules\ProductBrand\Http\Controllers;

//use App\Model\ModelMachine;
use Modules\ModelMachine\Entities\ModelMachine;
use Modules\ProductBrand\Entities\ProductBrand;
//use App\Model\ProductBrand;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
//use Google\Cloud\Storage;
use Storage;
class ProductBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    public function index()
    {
        $product_brands = ProductBrand::with('model_machines')->orderByDesc('id')->get();
        return view('admin.product_brand.main',compact('product_brands'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $model_machines = ModelMachine::all();
        return view('admin.product_brand.create',compact('model_machines'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {

//
//        $request->validate([
//            'name'=>'required',
//            'image' => 'required',
//            'desc'=>'required|longtext',
//
//        ]);
//        $googleDriveStorage = Storage::disk('google_drive');
//
//        $test = collect($googleDriveStorage->listContents('/', false))->where('type', '=', 'file')
//            ->where('filename', '=', pathinfo('slider_1.jpg', PATHINFO_FILENAME))
//            ->where('extension', '=', pathinfo('slider_1.jpg', PATHINFO_EXTENSION))->first();
//
//        $adapter = $googleDriveStorage->getDriver()->getAdapter();
//        //dd($adapter->getUrl('https://drive.google.com/file/d/1NpsMPr47aLzzQyBSvOwluQcLL_Y487FT/view'));
//
//        $result = $googleDriveStorage->url('/'.$test['filename']);
//
//        dd($result);
//        dd($request->all());
//        $product_brands = ProductBrand::find($request);
        //$path = null;
        $data = $request->all();
//        if ($request->file('image')) {
//            $googleDriveStorage = Storage::disk('google_drive');
//            $path = $request->file('image')
//            $contents = file_get_contents($request->file('image'));
//            dd($contents);
//            $googleDriveStorage->put($request->file('image'), $contents);
//            $data['image'] = $path;
//
//        }

        if($request->hasFile('image')) {
//            dd("Co' file");
            $fileName = time().'_'.$request->file('image')->getClientOriginalName();
            $filePath = $request->file('image');
            //$fileModel->name = time().'_'.$request->file->getClientOriginalName();
            $file_path = '/storage/' . $filePath;
            $fileTemp = public_path($file_path);

            //$weatherFake = json_decode(file_get_contents($fileTemp), true);
            //dd($file_path);



            $contents = file_get_contents($fileTemp);
            $googleDriveStorage = Storage::disk('google_drive');
//            dd($contents);
            $googleDriveStorage->put($fileName, $contents);

        }
        ProductBrand::create($data);
        dd($data);

        return  redirect('/admin/product_brand')->with('message', 'Created successfully !');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        ProductBrand::find($id)->delete();

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $product_brands = ProductBrand::findOrFail($id);
        $model_machines = ModelMachine::all();
        return view('admin.product_brand.edit',compact('product_brands','model_machines'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'price'=>'required',
            'price_old'=>'required',
            'desc'=>'required',
            'sale_off'=>'required',
            'status'=>'required',
            'image' => 'required',
        ]);

        $product_brands = ProductBrand::find($id);

        $path = null;
        if ($request->file('image')) {
            $path = $request->file('image')->store('public');
            @unlink('storage/'. $product_brands->image);
        }

        $data = $request->all();
        $data['image'] = $path;

//        dd($data);
        $product_brands->update($data);

        return redirect('/admin/product_brand');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        ProductBrand::find($id)->delete();

        return redirect()->back();
    }
}
