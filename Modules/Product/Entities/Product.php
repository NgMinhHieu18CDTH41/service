<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'id','name','model_machines_id'
    ];
    public function Model_machines()
    {
        return $this->belongsTo('Modules\ModelMachine\Entities\ModelMachine', 'model_machines_id');
    }


//    public function filterStatus($query, $model_machines)
//    {
//        return $query->where('name', $model_machines);
//    }
}
