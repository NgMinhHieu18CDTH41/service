<?php

namespace Modules\Product\Http\Controllers;
use Modules\ModelMachine\Entities\ModelMachine;
use Modules\Product\Entities\Product;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $products = Product::with('model_machines')->orderByDesc('id')->get();
        return view('admin.products.main',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $model_machines = ModelMachine::all();
        return view('admin.products.create',compact('model_machines'));    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',

        ]);
        $products = new Product($request->all());
        $products -> save();
        return  redirect('/admin/product')->with('message', 'Created successfully !');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        Product::find($id)->delete();

        return redirect()->back();    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $model_machines = ModelMachine::all();

        return view('admin.products.edit', compact('product','model_machines'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
        ]);
        $product = Product::find($id);
        $product-> update($request->all());
        return redirect('/admin/product');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->back();
    }
}
