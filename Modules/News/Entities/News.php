<?php

namespace Modules\News\Entities;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use Sortable;
    protected $table = 'news';
    protected $fillable = [
        'id','name','image','title',
    ];
    public $sortable = ['id', 'name', 'title', 'created_at', 'updated_at'];
//    static public function uploadAndResize($image, $width = 750, $height = null){
//        if(empty($image)) return;
//        $folder = "/images/news/";
//        if(!\Storage::disk(config('filesystems.disks.public.visibility'))->has($folder)){
//            \Storage::makeDirectory(config('filesystems.disks.public.visibility').$folder);
//        }
//        //getting timestamp
//        $timestamp = Carbon::now()->toDateTimeString();
//        $fileExt = $image->getClientOriginalExtension();
//        $filename = str_slug(basename($image->getClientOriginalName(), '.'.$fileExt));
//        $pathImage = str_replace([' ', ':'], '-', $folder.$timestamp. '-' .$filename.'.'.$fileExt);
//
//        $img = \Image::make($image->getRealPath())->resize($width, $height, function ($constraint) {
//            $constraint->aspectRatio();
//        });
//        $watermark = public_path('images/logo_50_200.png');
//        $img->insert($watermark, 'bottom-right');
//        $img->save(public_path('storage/').$pathImage);
//
//        return config('filesystems.disks.public.visibility').$pathImage;
//    }
}
