<?php

namespace Modules\Category\Http\Controllers\Api;

use Modules\ModelMachine\Entities\ModelMachine;
use Modules\Category\Entities\Category;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Category\Transformers\CategoryResource;

class CategoryController extends Controller
{
    /**
     * Display a listing of the reponse.
     * @return Renderable
     */
    public function index()
    {
//        return api_success(array('data'=>Category::with('model_machines')->orderByDesc('id')->get())
//    );
        header('Access-Control-Allow-Origin: *');
        $categories = Category::with('model_machines')->orderByDesc('id')->get();
        return CategoryResource::collection($categories);
    }

    public function findID($id)
    {
        header('Access-Control-Allow-Origin: *');
        $category = Category::with('model_machines')->find($id);
        return new CategoryResource($category);
    }
}

