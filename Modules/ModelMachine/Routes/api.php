<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/model_machine', function (Request $request) {
    return $request->user();
});
Route::prefix('model_machine')->group( function () {
    Route::get('/', 'Api\ModelMachineController@index');
    Route::get('/find/{id}', 'Api\ModelMachineController@findID');
});
