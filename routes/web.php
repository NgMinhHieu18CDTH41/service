<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// document
Route::get('create_folder','StorageController@createfolder');
Route::get('RenameFolder','StorageController@renamefolder');
Route::get('DeleteFolder','StorageController@deletefolder');
Route::get('UploadImage', 'StorageController@uploadimage');
Route::get('ListDocument', 'StorageController@list_document');

//folder








Route::get('/', 'FontendController@index')->name('trang_chu');
Route::get('/home',function (){
    return view('app');
});
Route::get('admin', function () {
    return view('admin.dashboard.main');

});
Route::post('/updateAvatar', 'UserAvatarController@update');
//Route::get('/', function () {
//    return view('app');
//
//});
    //Route::get('/list-categories', function() {
    //    header('Access-Control-Allow-Origin: *');
    //
    //    $listCategories = \App\Model\ModelMachine::with('products')->orderByDesc('id')->get();
    //
    ////    if
    //    return response()->json($listCategories);
    //    // dd($listCategories);
    //
    //});
//Route::get('search/{categories_id}/{model_machines_name}','ModelMachineController@search');

//Auth::guard('venue')->user();
//Auth::route();

//Route::post('admin1',function (){
// return 'you are admin';
//})

//Route::get('login',function (){
//   return view('auth.login')->name('home');
//});

Auth::routes();

Route::get('/login','LoginController@index')->name('customLogin');
Route::get('/logout','LoginController@logout')->name('logout');
Route::post('/login','LoginController@authenticate')->name('checkLogin');
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tours', 'SearchController@index')->name('search.index');
//Route::get('search-results', 'SearchController@search')->name('search.result');
