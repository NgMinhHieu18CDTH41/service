<?php

use Illuminate\Http\Request;
use Modules\Category\Http\Controllers\Api\CategoryController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::namespace('Api')->group(function() {
////    Route::get('brand', 'BrandController@index');
////    Route::get('brand/{id}', 'BrandController@show')->where('id', '[0-9]+');
////    Route::get('brand/{id}/search', 'BrandController@search');
////    Route::get('brand/create', 'BrandController@create');
////    Route::get('brand/{id}', 'BrandController@destroy');
////
////    Route::get('product', 'ProductController@index');
////    Route::get('product/{id}', 'ProductController@show')->where('id', '[0-9]+');
////    Route::get('product/search', 'ProductController@search')->where('id', '[0-9]+');
////    Route::get('product/create', 'ProductController@create');
////    Route::get('product/{id}', 'ProductController@destroy');
//
//    Route::get('category', 'CategoryController@index');
//    Route::get('category/{id}', 'CategoryController@show')->where('id', '[0-9]+');
//    Route::get('category/search', 'CategoryController@search')->where('id', '[0-9]+');
//    Route::get('category/create', 'CategoryController@create');
//    Route::get('category/{id}', 'CategoryController@destroy');
//
//});
