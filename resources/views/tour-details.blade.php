<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" href="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/favicon.png?1607937814596"
          type="image/x-icon"/>
    <link rel="stylesheet" href="{{asset('fontend/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"/>
    <link rel="stylesheet" href="{{asset('fontend/css/reponsive.css')}}">
    <link rel="stylesheet" href="{{asset('fontend/css/style.css')}} "/>
    <link rel="stylesheet" href="{{asset('fontend/css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('fontend/css/owl.theme.css')}}">

    <title>H NEWS</title>
</head>

<body>
<div id="main">
@include('layouts.header')
{{--@include('layouts.slider')--}}
<!--slider-->
    <hr>
    <div class="tours">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 detail-product">
                    <div class="row margin-bottom-10 margin-bottom-20">
                        <div class="col-md-6">
                            <div id="sync1" class="owl-carousel owl-theme not-dqowl owl-loaded owl-drag">
                                <div class="owl-stage-outer owl-height" style="height: 370px;">
                                    <div class="owl-stage"
                                         style="transform: translate3d(-3330px, 0px, 0px); transition: all 0.25s ease 0s; width: 6105px;">
                                        <div class="owl-item cloned" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/los-angeles-santa-monica-place.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/nomad.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/optimized-f4bd40aca74f672f0141020134218371.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/83864b64404979-5ad0e1bdba9b2.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/08.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/los-angeles-santa-monica-place.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item active" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/nomad.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/optimized-f4bd40aca74f672f0141020134218371.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/83864b64404979-5ad0e1bdba9b2.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/08.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item cloned" style="width: 555px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/grande/100/299/077/products/los-angeles-santa-monica-place.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-nav">
                                    <div class="owl-prev">prev</div>
                                    <div class="owl-next">next</div>
                                </div>
                                <div class="owl-dots disabled"></div>
                            </div>

                            <div id="sync2" class="owl-carousel owl-theme not-dqowl owl-loaded owl-drag">
                                <div class="owl-stage-outer">
                                    <div class="owl-stage"
                                         style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 565px;">
                                        <div class="owl-item active" style="width: 103px; margin-right: 10px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/medium/100/299/077/products/83864b64404979-5ad0e1bdba9b2.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item active" style="width: 103px; margin-right: 10px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/medium/100/299/077/products/08.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item active" style="width: 103px; margin-right: 10px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/medium/100/299/077/products/los-angeles-santa-monica-place.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item active current" style="width: 103px; margin-right: 10px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/medium/100/299/077/products/nomad.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                        <div class="owl-item active" style="width: 103px; margin-right: 10px;">
                                            <div class="item">
                                                <img
                                                    src="//bizweb.dktcdn.net/thumb/medium/100/299/077/products/optimized-f4bd40aca74f672f0141020134218371.jpg?v=1529553163227"
                                                    alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]"
                                                    class="img-responsive center-block">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="owl-nav disabled">
                                    <div class="owl-prev">prev</div>
                                    <div class="owl-next">next</div>
                                </div>
                                <div class="owl-dots disabled"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="details-pro">
                                <h1 class="title-head">Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios
                                    Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]</h1>

                                {{--                                <div class="sku-product hidden">--}}
                                {{--                                    SKU: <span class="variant-sku" itemprop="sku" content="Đang cập nhật">(Đang cập nhật...)</span>--}}
                                {{--                                    <span class="hidden" itemprop="brand" itemscope=""--}}
                                {{--                                          itemtype="https://schema.org/brand">Ant Du lịch</span>--}}
                                {{--                                </div>--}}

                                <div class="journey">
                                    <span>Hành trình:</span> Hồ Chí Minh - Mỹ
                                </div>

                                <ul class="ct_course_list">


                                    <li>
                                        <div class="ulimg"><img
                                                src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1610202220100"
                                                alt="Di chuyển bằng Ô tô"></div>
                                        Di chuyển bằng Ô tô
                                    </li>


                                    <li>
                                        <div class="ulimg"><img
                                                src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1610202220100"
                                                alt="Di chuyển bằng máy bay"></div>
                                        Di chuyển bằng máy bay
                                    </li>


                                    <li>
                                        <div class="ulimg"><img
                                                src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1610202220100"
                                                alt="Thứ 2 - 7 hằng tuần"></div>
                                        <span id="date-khoi-hanh">Thứ 2 - 7 hằng tuần</span>
                                    </li>


                                    <li>
                                        <div class="ulimg"><img
                                                src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1610202220100"
                                                alt="6 ngày 5 đêm"></div>
                                        6 ngày 5 đêm
                                    </li>


                                </ul>

                                <div class="product-summary product_description margin-bottom-10 margin-top-5">
                                    <div class="rte description">

                                        <p>- Khám phá Bờ Tây nước Mỹ chứa đựng vô vàn thú vị với mức giá siêu tiết
                                            kiệm.</p>

                                        <p>- Trải nghiệm 2 đêm khách sạn 5 sao Bellagio nổi tiếng Las Vegas</p>

                                        <p>- Hòa mình vào không khí trường quay hoành tráng tại Universal Studios, trung
                                            tâm kinh đô điện ảnh Hollywood</p>

                                        <p>- Ghé thăm khu Little Saigon - nơi sinh sống của cộng đồng người Việt.</p>

                                    </div>
                                </div>

                                <div class="call-me-back">
                                    <ul class="row">
                                        <li class="col-md-6 col-sm-6 col-xs-6 col-100">
                                            <a href="#book-tour-now" title="Đặt tour" class="icon-mouse-scroll"
                                               style="text-decoration: none">
                                                <i class="fas fa-paper-plane" aria-hidden="true"></i> Đặt tour
                                            </a>
                                        </li>
                                        <li class="col-md-6 col-sm-6 col-xs-6 col-100">
                                            <button class="btn-callmeback" type="button" data-toggle="modal"
                                                    data-target="#myModal"><i class="fas fa-phone"
                                                                              aria-hidden="true"></i> Gọi lại cho tôi
                                                sau
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="book-tour-now">
                        <div class="col-xs-12 col-sm-12 col-md-7 details-pro">
                         
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-5 tour-policy">

                            <div class="tour-policy-content">


                                <div class="main-project__tab--content tour-no-content">
                                    <div class="product-promotions-list">
                                        <h2 class="product-promotions-list-title">Chính sách Tour</h2>
                                        <div class="product-promotions-list-content">
                                            Chính sách đang được cập nhật.
                                        </div>
                                    </div>
                                </div>


                                <div class="main-project__tab--content">
                                    <div class="product-promotions-list">
                                        <h2 class="product-promotions-list-title">Chính sách Tour</h2>
                                        <div class="product-promotions-list-content">

                                            <p><strong>* Giá tour bao gồm:</strong><br>
                                                - Vé máy bay khứ hồi theo đoàn. Thuế sân bay hai đầu và phí an ninh.
                                                Chuyến bay và giờ bay có thể thay đổi giờ chót tùy thuộc vào tình hình
                                                hàng không. Dự kiến hàng không Hong Kong Airlines (HX) hoặc các hàng
                                                không khác có thể đáp ứng thuận tiện vé cho đoàn tại thời điểm khởi
                                                hành.<br>
                                                - Hướng dẫn suốt tuyến. 2 đêm khách sạn 5 sao tại Las Vegas, 2 đêm khách
                                                sạn 3 sao tại Los Angeles (2 người/phòng), các bữa ăn theo chương trình.<br>
                                                - Xe đưa đón và tham quan theo chương trình, quà lưu niệm (balo,
                                                nón).<br>
                                                - Chi phí tham quan và vé vào cửa tham quan theo chương trình. Tiền thuê
                                                xe đẩy hành lý tại sân bay theo đoàn (2 khách người lớn/1 xe).<br>
                                                - Thuế giá trị gia tăng.</p>
                                            <p><strong>* Giá tour không bao gồm:</strong><br>
                                                - Chi phí làm visa Mỹ: 3.680.000 vnđ/khách (giá có thể thay đổi theo
                                                thực tế), chi phí làm hộ chiếu, các chương trình tự chọn, nước uống,
                                                giặt ủi, điện thoại... và các chi phí cá nhân khác của khách ngoài
                                                chương trình.<br>
                                                - Hành lý quá cước, chi phí dời ngày và đổi chặng bay theo qui định của
                                                hàng không.<br>
                                                - Phí phòng đơn (dành cho khách yêu cầu ở phòng đơn).<br>
                                                - Tiền bồi dưỡng cho HDV và lái xe địa phương (06 USD/ khách/ngày).</p>
                                            <p><strong>THÔNG TIN HƯỚNG DẪN</strong></p>
                                            <p><strong>Quy trình đăng ký và thanh toán:</strong></p>
                                            <p>1.Đợt 1: Đặt cọc 25.000.000 vnđ/khách và nộp một bản copy hồ sơ theo yêu
                                                cầu.</p>
                                            <p>2.Đợt 2: Thanh toán số tiền tour còn lại trong vòng 3 ngày làm việc khi
                                                được chấp thuận visa.<br>
                                                - Điều khoản hủy tour (Thời gian hủy tour được tính cho ngày làm việc,
                                                không tính ngày Thứ Bảy, Chủ Nhật và các ngày nghỉ Lễ).<br>
                                                - Sau khi đặt cọc tour, nếu Qúy khách báo hủy tour Công ty chúng tôi sẽ
                                                không hoàn lại tiền cọc. Đồng thời chúng tôi&nbsp;sẽ báo hủy hồ sơ phỏng
                                                vấn của khách.<br>
                                                - Nếu quý khách bị từ chối visa Mỹ, quý khách vui lòng nộp lệ phí là:
                                                3.680.000 vnđ.<br>
                                                - Nếu quý khách báo hủy tour vui lòng thanh toán lệ phí hủy tour&nbsp;
                                                cụ thể như sau:</p>
                                            <p>1/ Trước ngày đi từ 35 - 20 ngày làm việc (không tính thứ Bảy &amp; Chủ
                                                Nhật &amp; ngày nghỉ Tết) thanh toán: 75% giá tour</p>
                                            <p>2/ Trước ngày đi từ 10 - 19 ngày làm việc (không tính thứ Bảy &amp; Chủ
                                                Nhật &amp; ngày nghỉ Tết) thanh toán: 90% giá tour</p>
                                            <p>3/ Hủy trước ngày đi trong vòng 9 ngày làm việc (không tính thứ Bảy &amp;
                                                Chủ Nhật &amp; ngày nghỉ Tết): 100% giá tour<br>
                                                - Quý khách có nhu cầu lưu trú lại Mỹ thêm ngoài chương trình tour vui
                                                lòng thông báo tại thời điểm đăng kí và đóng thêm tiền vé máy bay phụ
                                                trội ở lại về sau và các chặng bay nội địa theo quy định của hàng không.<br>
                                                - Trường hợp đoàn không đáp ứng được số lượng khách đạt visa tối thiểu
                                                10 người,&nbsp;có quyền hủy dịch vụ và chuyển ngày khởi hành sang thời
                                                điểm khác. Quý khách vui lòng sắp xếp tham gia tour trong thời hạn visa
                                                hợp lệ. Trường hợp quý khách không tham gia tour,&nbsp;sẽ chỉ thu
                                                10.000.000 vnđ và hoàn trả toàn bộ phần tiền còn lại cho quý khách.</p>
                                        </div>
                                    </div>
                                </div>
                                <style>
                                    .tour-no-content {
                                        display: none;
                                    }
                                </style>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
@include('layouts.footer')
<!--footer-->
</div>
<!--main-->
<div class="backdrop__body-backdrop___1rvky active"></div>
<ul class="the-article-tools">
    <li class="btnZalo zalo-share-button ">
        <a target="_blank " href="http://zalo.me/0965 198 897 " title="Chat qua Zalo">
            <span class="ti-zalo "></span>
        </a>
        <span class="label ">Chat qua Zalo</span>
    </li>
    <li class="btnFacebook ">
        <a target="_blank" href="https://www.messenger.com/0609nmh " title="Chat qua Messenger">
            <span class="ti-facebook "></span>
        </a>
        <span class="label">Chat qua Messenger</span>
    </li>
    <li class="btnphone ">
        <button type="button " data-toggle="modal" data-target="#hotlineModal">
            <span class="fas fa-phone"></span>
        </button>
        <span class="label ">Hotline đặt Tour</span>
    </li>
</ul>
</body>
<script src="{{asset('fontend/js/script.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js "
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW "
        crossorigin="anonymous "></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js "></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js "></script>
<script src="{{asset('fontend/js/main.js')}}"></script>
<script src="{{asset('fontend/js/owl.carousel.min.js')}}"></script>
</html>
