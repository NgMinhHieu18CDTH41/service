@extends('admin.layouts.app')
@section('title', 'Danh sach the loai')
@section('content')
    <div class="add"style="padding: 12px">
        <button class="btn btn-success" title="add news"><a href="{{URL::to('/admin/product_brand/create')}}"><i class="fas fa-plus"></i> Add ProductBrand</a></button>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Name</th>
            <th scope="col">@sortablelink('price')</th>
            <th scope="col">@sortablelink('price_old')</th>

            <th scope="col">Desc</th>
            <th scope="col">Sale off</th>
            <th scope="col">Status</th>
            <th scope="col">image</th>
            <th scope="col">model_machines_id</th>
            <th>@sortablelink('created_at')<i class="fas fa-sort-numeric-down-alt"></i></th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
        @forelse($product_brands as $bp)
            <tr>
                <th class="stt1"style="width: 50px;">{{ $stt }}</th>
                <td><a>{{str_limit($bp->name, 20)}}</a></td>
                <td><a class="text-bold text-danger" >{{(number_format($bp->price))}} </a></td>
                <td><a  class="text-bold text-danger" >{{(number_format($bp->price_old))}}</a></td>

                <td><a >{!!str_limit($bp->desc,20)!!}</a></td>
                <td><a>{{$bp->sale_off }}</a></td>
                <td><a >{{$bp->status}}</a></td>
                <td><img  width="40" height="40" src="{{ asset('storage'. str_replace('public', '', $bp->image))}}"></td>
                <td>{{ ($bp->model_machines->name) }}</td>
                <td>{{ $bp->created_at->format('d-m-Y') }}</td>
                <td>
                    <button class="btn btn-primary"><a href="{{route('product_brand.edit',['id'=>$bp->id])}}"><i class="fas fa-pencil-alt " style="color: white"></i></a></button>
                    <button class="btn btn-danger"><a href="{{ route('product_brand.destroy', ['id' => $bp->id]) }}"><i class="fas fa-trash-alt" style="color: white"></i></a></button>
{{--                    <button class="btn btn-warning"><a href="{{route('productbrand.edit',['id'=>$bp->id])}}"><i class="fas fa-plus-circle " style="color: white"></i></a></button>--}}
                </td>

            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
    <style>
        td {
            width: calc((1130px)/11);
        }
        .add a {
            color: white;
        }
        table.table {
            background: cornsilk;
        }
    </style>
@stop

