@extends('admin.layouts.app')
@section('title', 'Danh sach the loai')
@section('content')
    <div class="add"style="padding: 12px">
        <button class="btn btn-success" title="add news"><a href="{{URL::to('/admin/news/create')}}"><i class="fas fa-plus"></i> Add News</a></button>
    </div>

    <table class="table">
        <thead>
        <tr>
{{--            <th><input  id="selectAll" type="checkbox"></th>--}}
            <th scope="col">STT</th>
            <th scope="col">@sortablelink('name')</th>
            <th scope="col">@sortablelink('title')</th>
            <th scope="col">Image</th>
            <th>@sortablelink('created_at')<i class="fas fa-sort-numeric-down-alt"></i></th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php  $stt = 1;?>
        @forelse($news as $n)
{{--            <tr> <td><input  class="checkboxes" onclick() type="checkbox"></td>--}}
                <th  class="stt1"style="width: 50px;">{{ $stt }}</th>
                <td ><a >{{str_limit($n->name ,50)}}</a></td>
                <td><a >{!!str_limit ($n->title,100)  !!}</a></td>
                <td><img  width="120" src="{{ asset('storage'. str_replace('public', '', $n->image))}}"></td>
                <td>{{ $n->created_at->format('d-m-Y') }}</td>
                <td>
                    <button class="btn btn-primary" title="Edit"><a href="{{route('news.edit',['id'=>$n->id])}}"><i class="fas fa-pencil-alt " style="color: white"></i></a></button>
                    <button class="btn btn-danger" title="Delete"><a href="{{ route('news.destroy', ['id' => $n->id]) }}"><i class="fas fa-trash-alt" style="color: white"></i></a></button>
                </td>

            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
    <style>
        td {
            width: calc((1130px)/5);
        }
    </style>
@section('script')
    <script src=""></script>
    <script>
        $(function(){
        $("#checkAll").onclick(function(){
            if (this.checked) {
                $(".checkboxes").prop("checked", true);
            } else {
                $(".checkboxes").prop("checked", false);
            }
            console.log('124');
        });
        });
    </script>
@endsection
@stop

