<div class="service1">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <div class="tour-new ">
                    <h2>Tour
                        <span>Mới Nhất</span>
                    </h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <p>Hệ thống đặt Tour hàng đầu Việt Nam. Hơn 300 tours du lịch ở Việt Nam và Quốc tế</p>
                </div>

            </div>
        </div>
        <div class="row section_tour-new">
            <div class="col-xl-4 col-sm-6 col-xs-6">
                <div class="product-box box-services">
                    <div class="product-thumbnail ">
                        <a href=" " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/83864b64404979-5ad0e1bdba9b2.jpg?v=1529553163000 " alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                        </a>
                        <div class="sale-off ">
                            -9%
                        </div>
                    </div>
                    <div class="product-inf a-left  padding-10">
                        <h3 class="color-black font-16">
                            <a class="line-clamp " href="# " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]</a>
                        </h3>
                        <div class="price-tag">
                            <div class="box-prices ">
                                <div class="price-box clearfix ">
                                    <div class="special-price f-left ">
                                        <span class="price product-price ">49.000.000₫</span>
                                    </div>
                                    <div class="old-price ">
                                        <span class="price product-price-old ">
                                        54.000.000₫			
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tag ">
                                <ul class="ct_course_list ">
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                    </li>

                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box-date-tour ">
                            <ul class="ct_course_list ">
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                </li>
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-xs-6">
                <div class="product-box  box-services">
                    <div class="product-thumbnail ">
                        <a href=" " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            <img src="https://bizweb.dktcdn.net/thumb/large/100/299/077/products/1-large1.jpg?v=1529553697103 " alt="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long ">
                        </a>

                    </div>
                    <div class="product-inf a-left  padding-10">
                        <h3 class="color-black font-16">
                            <a class="line-clamp " href="# " title="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long ">
                            Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long</a>
                        </h3>
                        <div class="price-tag ">
                            <div class="box-prices ">
                                <div class="price-box clearfix ">
                                    <div class="special-price f-left ">
                                        <span class="price product-price ">49.000.000₫</span>
                                    </div>

                                </div>
                            </div>
                            <div class="box-tag ">
                                <ul class="ct_course_list ">
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                    </li>

                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box-date-tour ">
                            <ul class="ct_course_list ">
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                </li>
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-xs-6">
                <div class="product-box  box-services">
                    <div class="product-thumbnail ">
                        <a href=" " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            <img src="https://bizweb.dktcdn.net/thumb/large/100/299/077/products/grand-britain-europe-tour-5-min.jpg?v=1529553857067 " alt="Du lịch Châu Âu Pháp - Thụy Sỹ - Núi Jungfrau - Ý ">
                        </a>

                    </div>
                    <div class="product-inf a-left  padding-10">
                        <h3 class="color-black font-16">
                            <a class="line-clamp " href="# " title="Du lịch Châu Âu Pháp - Thụy Sỹ - Núi Jungfrau - Ý ">
                            Du lịch Châu Âu Pháp - Thụy Sỹ - Núi Jungfrau - Ý</a>
                        </h3>
                        <div class="price-tag ">
                            <div class="box-prices ">
                                <div class="price-box clearfix ">
                                    <div class="special-price f-left ">
                                        <span class="price product-price ">49.000.000₫</span>
                                    </div>


                                </div>
                            </div>
                            <div class="box-tag ">
                                <ul class="ct_course_list ">
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                    </li>
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng tàu thủy ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1607937814596 " alt="Di chuyển bằng tàu thủy ">
                                    </li>
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box-date-tour ">
                            <ul class="ct_course_list ">
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                </li>
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-xs-6 ">
                <div class="product-box  box-services">
                    <div class="product-thumbnail ">
                        <a href=" " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            <img src="https://bizweb.dktcdn.net/thumb/large/100/299/077/products/0r2a5723.jpg?v=1529553943837 " alt="Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof] ">
                        </a>
                        <div class="sale-off ">
                            9%
                        </div>
                    </div>
                    <div class="product-inf a-left  padding-10">
                        <h3 class="color-black font-16">
                            <a class="line-clamp " href="# " title="Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof] ">
                            Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof]</a>
                        </h3>
                        <div class="price-tag ">
                            <div class="box-prices ">
                                <div class="price-box clearfix ">
                                    <div class="special-price f-left ">
                                        <span class="price product-price ">49.000.000₫</span>
                                    </div>
                                    <div class="old-price ">
                                        <span class="price product-price-old ">
                                    54.000.000₫			
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tag ">
                                <ul class="ct_course_list ">
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                    </li>
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng tàu thủy ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1607937814596 " alt="Di chuyển bằng tàu thủy ">
                                    </li>
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box-date-tour ">
                            <ul class="ct_course_list ">
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                </li>
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-xs-6 ">
                <div class="product-box  box-services">
                    <div class="product-thumbnail ">
                        <a href=" " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            <img src="https://bizweb.dktcdn.net/thumb/large/100/299/077/products/53916-1315037279.jpg?v=1529554090113 " alt="Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế ">
                        </a>
                        <div class="sale-off ">
                            -3%
                        </div>
                    </div>
                    <div class="product-inf a-left  padding-10">
                        <h3 class="color-black font-16">
                            <a class="line-clamp " href="# " title="Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế ">
                            Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế</a>
                        </h3>
                        <div class="price-tag">
                            <div class="box-prices ">
                                <div class="price-box clearfix ">
                                    <div class="special-price f-left ">
                                        <span class="price product-price ">6.300.000₫</span>
                                    </div>
                                    <div class="old-price ">
                                        <span class="price product-price-old ">
                                 6.500.000₫		
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="box-tag ">
                                <ul class="ct_course_list ">
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                    </li>

                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box-date-tour ">
                            <ul class="ct_course_list ">
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                </li>
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6 col-xs-6 ">
                <div class="product-box  box-services">
                    <div class="product-thumbnail ">
                        <a href=" " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                            <img src="https://bizweb.dktcdn.net/thumb/large/100/299/077/products/anam-resort-nha-trang-vietnam-23.jpg?v=1529554176777 " alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio,
                    Las Vegas] ">
                        </a>

                    </div>
                    <div class="product-inf a-left  padding-10">
                        <h3 class="color-black font-16">
                            <a class="line-clamp " href="# " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                        Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]</a>
                        </h3>
                        <div class="price-tag">
                            <div class="box-prices ">
                                <div class="price-box clearfix ">
                                    <div class="special-price f-left ">
                                        <span class="price product-price ">3.300.000₫</span>
                                    </div>

                                </div>
                            </div>
                            <div class="box-tag">
                                <ul class="ct_course_list ">
                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                    </li>

                                    <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="box-date-tour ">
                            <ul class="ct_course_list ">
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                </li>
                                <li class="clearfix ">
                                    <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>