<div class="service2">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="tour-new ">
                    <h2>Tour
                        <span>Trong Nước</span>
                    </h2>
                    <div class="title-line ">
                        <div class="tl-1 "></div>
                        <div class="tl-2 "></div>
                        <div class="tl-3 "></div>
                    </div>
                    <p>Tour du lịch Nước ngoài tại Ant Du lịch. Du lịch 5 châu - Trải nghiệm sắc xuân thế giới</p>
                </div>
            </div>
        </div>
        <section class="tab-affiliate-marketing ">
            <div class="container">
                <ul class="nav nav-tabs  text-center">
                    <li class="active"><a class="active color-b3" href="#midside" data-toggle="tab"><span>Miền Trung</span></a></li>
                    <li><a class="color-b3" href="#northside" data-toggle="tab">Miền Bắc</a></li>
                    <li><a class="color-b3" href="#southside" data-toggle="tab">Miền Nam</a></li>
                </ul>
                <ul class="tabs tabs-title tab-mobile  clearfix hidden-sm hidden-md hidden-lg text-center">
                    <li class="prev current"><i class="fas fa-angle-left"></i></li>
                    <li class="tab-link tab-title hidden-sm hidden-md hidden-lg current tab-titlexs" href="#northside" data-toggle="tab">

                        <span>Miền Trung</span>

                    </li>
                    <li class="next"><i class="fas fa-angle-right"></i></li>
                </ul>
            </div>
            <div class="tab-affiliate-marketing-bg">
                <div class="tab-content" id="tabs">
                    <div class="tab-pane active img_before" id="midside">
                        <div class="container">
                            <div class="row row-onclick-ads">
                                <div class="multiple-items">
                                    <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                        <div class="product-box  box-services">
                                            <div class="product-thumbnail ">
                                                <a href="/du-lich-da-nang-kdl-ba-na-hoi-an-co-do-hue" title="Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế">
                                                    <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/53916-1315037279.jpg?v=1529554090113" alt="Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế">
                                                </a>
                                                <div class="sale-off ">
                                                    -3%
                                                </div>
                                            </div>
                                            <div class="product-inf a-left  padding-10">
                                                <h3 class="color-black font-16">
                                                    <a class="line-clamp " href="# " title="Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế">
                                                        Du lịch Đà Nẵng - KDL Bà Nà - Hội An - Cố Đô Huế</a>
                                                </h3>
                                                <div class="price-tag ">
                                                    <div class="box-prices">
                                                        <div class="price-box clearfix">
                                                            <div class="special-price f-left">
                                                                <span class="price product-price">6.300.000₫</span>
                                                            </div>

                                                            <div class="old-price">
                                                                <span class="price product-price-old">
                                                                    6.500.000₫			
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-tag ">
                                                        <ul class="ct_course_list ">
                                                            <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                            </li>

                                                            <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="box-date-tour ">
                                                    <ul class="ct_course_list ">
                                                        <li class="clearfix ">
                                                            <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                        </li>
                                                        <li class="clearfix ">
                                                            <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                        <div class="product-box  box-services">
                                            <div class="product-thumbnail">
                                                <a href="/du-lich-hue-ho-truoi-da-nang-suoi-khoang-nong-nui-than-tai-kdl-ba-na" title="Du lịch Huế - Hồ Truồi - Đà Nẵng - Suối Khoáng Nóng Núi Thần Tài - KDL Bà Nà">
                                                    <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/bana-hills.jpg?v=1529554384043" alt="Du lịch Huế - Hồ Truồi - Đà Nẵng - Suối Khoáng Nóng Núi Thần Tài - KDL Bà Nà">
                                                </a>
                                                <div class="sale-off">- 5%
                                                </div>
                                            </div>
                                            <div class="product-inf a-left  padding-10">
                                                <h3 class="color-black font-16">
                                                    <a class="line-clamp " href="# " title="Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM] ">
                                                        Du lịch Huế - Hồ Truồi - Đà Nẵng - Suối Khoáng Nóng Núi Thần Tài - KDL Bà Nà</a>
                                                </h3>
                                                <div class="price-tag">
                                                    <div class="box-prices ">
                                                        <div class="price-box clearfix ">
                                                            <div class="special-price f-left ">
                                                                <span class="price product-price ">3.579.000₫</span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="box-tag ">
                                                        <ul class="ct_course_list ">
                                                            <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                            </li>
                                                            <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Di chuyển bằng tàu thủy">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1610202220100" alt="Di chuyển bằng tàu thủy">
                                                            </li>
                                                            <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="box-date-tour ">
                                                    <ul class="ct_course_list ">
                                                        <li class="clearfix ">
                                                            <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                        </li>
                                                        <li class="clearfix ">
                                                            <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                        <div class="product-box  box-services">
                                            <div class="product-thumbnail">
                                                <a href="/du-lich-nha-trang-hon-lao-doc-let" title="Du lịch Nha Trang - Hòn Lao - Dốc Lết">
                                                    <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/ponagar-cham-temple-nha-trang-min1.jpg?v=1529554507043" alt="Du lịch Nha Trang - Hòn Lao - Dốc Lết">
                                                </a>
                                                <div class="sale-off">- 22%
                                                </div>
                                            </div>
                                            <div class="product-inf a-left  padding-10">
                                                <h3 class="color-black font-16">
                                                    <a class="line-clamp " href="# " title="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long ">
                                                        Du lịch Nha Trang - Hòn Lao - Dốc Lết</a>
                                                </h3>
                                                <div class="price-tag">
                                                    <div class="box-prices ">
                                                        <div class="price-box clearfix ">
                                                            <div class="special-price f-left ">
                                                                <span class="price product-price ">3.400.000₫</span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="box-tag ">
                                                        <ul class="ct_course_list ">
                                                            <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                            </li>

                                                            <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                                <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="box-date-tour ">
                                                    <ul class="ct_course_list ">
                                                        <li class="clearfix ">
                                                            <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                        </li>
                                                        <li class="clearfix ">
                                                            <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane img_before" id="northside">
                        <div class="container">
                            <div class="row row-onclick-ads">
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail">
                                            <a href="/du-lich-ha-noi-lao-cai-sapa-ha-long" title="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/1-large1.jpg?v=1529553697103" alt="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long">
                                            </a>
                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                                                    Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">49.000.000₫</span>
                                                        </div>
                                                        <div class="old-price ">
                                                            <span class="price product-price-old ">
                                                            54.000.000₫			
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail">
                                            <a href="/du-lich-cao-bang-ban-gioc-bac-kan-ba-be-ha-noi" title="Du lịch Cao Bằng - Bản Giốc - Bắc Kạn - Ba Bể - Hà Nội">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/chiem-nguon-ve-dep-cong-vien-dia-chat-cao-bang-1.jpg?v=1529554586100" alt="Du lịch Cao Bằng - Bản Giốc - Bắc Kạn - Ba Bể - Hà Nội">
                                            </a>
                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM] ">
                                                    Du lịch Cao Bằng - Bản Giốc - Bắc Kạn - Ba Bể - Hà Nội</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.579.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Di chuyển bằng tàu thủy">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1610202220100" alt="Di chuyển bằng tàu thủy">
                                                        </li>
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail">
                                            <a href="/du-lich-ha-noi-ninh-binh-cat-ba-ha-long-hai-duong" title="Du lịch Hà Nội - Ninh Bình - Cát Bà - Hạ Long - Hải Dương">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/jed1367636408.jpg?v=1529554705153" alt="Du lịch Hà Nội - Ninh Bình - Cát Bà - Hạ Long - Hải Dương">
                                            </a>
                                            <div class="sale-off">- 15%
                                            </div>
                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM] ">
                                                    Du lịch Cao Bằng - Bản Giốc - Bắc Kạn - Ba Bể - Hà Nội</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.579.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Di chuyển bằng tàu thủy">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1610202220100" alt="Di chuyển bằng tàu thủy">
                                                        </li>
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane img_before" id="southside">
                        <div class="container">
                            <div class="row row-onclick-ads">
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail">
                                            <a href="/du-lich-kham-pha-cai-be-can-tho-chau-doc-ha-tien" title="Du lịch khám phá Cái Bè - Cần Thơ - Châu Đốc - Hà Tiên">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/bai-tam-hon-rom.jpg?v=1529553447977" alt="Du lịch khám phá Cái Bè - Cần Thơ - Châu Đốc - Hà Tiên">
                                            </a>
                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch khám phá Cái Bè - Cần Thơ - Châu Đốc - Hà Tiên ">
                                                    Du lịch khám phá Cái Bè - Cần Thơ - Châu Đốc - Hà Tiên</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">49.000.000₫</span>
                                                        </div>
                                                        <div class="old-price ">
                                                            <span class="price product-price-old ">
                                                            54.000.000₫			
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail">
                                            <a href="/du-lich-phu-quoc-bai-sao-vinpearl-land" title="Du lịch Phú Quốc - Bãi Sao - Vinpearl Land">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/3578033879.jpg?v=1529553550187" alt="Du lịch Phú Quốc - Bãi Sao - Vinpearl Land">
                                            </a>
                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long ">
                                                    Du lịch Phú Quốc - Bãi Sao - Vinpearl Land</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">4.230.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail">
                                            <a href="/du-lich-phu-quoc-cau-ca-ngam-san-ho" title="Du lịch Phú Quốc Câu cá - Ngắm san hô">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/san-golf-9-lo.jpg?v=1529554845807" alt="Du lịch Phú Quốc Câu cá - Ngắm san hô">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Phú Quốc Câu cá - Ngắm san hô">
                                                    Du lịch Phú Quốc Câu cá - Ngắm san hô</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.579.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Di chuyển bằng tàu thủy">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1610202220100" alt="Di chuyển bằng tàu thủy">
                                                        </li>
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </section>
        <div class="owl-nav disabled">
            <div class="owl-prev disabled">prev</div>
            <div class="owl-next disabled">next</div>
        </div>
        <div class="owl-dots disabled">
            <div class="owl-dot active"><span></span></div>
        </div>
    </div>
</div>