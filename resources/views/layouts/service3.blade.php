<div class="service3">
    <div class="container ">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="tour-new ">
                    <h2>Tour
                        <span>Nước Ngoài</span>
                    </h2>
                    <div class="title-line ">
                        <div class="tl-1 "></div>
                        <div class="tl-2 "></div>
                        <div class="tl-3 "></div>
                    </div>
                    <p>Tour du lịch Nước ngoài tại Ant Du lịch. Du lịch 5 châu - Trải nghiệm sắc xuân thế giới</p>
                </div>
            </div>
        </div>
        <section class="tab-affiliate-marketing ">
            <div class="container">
                <ul class="nav nav-tabs   text-center">
                    <li class="active"><a class="active color-b3" href="#Asia" data-toggle="tab"><span>Du lịch Châu Á</span></a></li>
                    <li><a class="color-b3" href="#Europe" data-toggle="tab">Du lịch Châu Âu</a></li>
                    <li><a class="color-b3" href="#Australia" data-toggle="tab">Du lịch Châu Úc</a></li>
                    <li><a class="color-b3" href="#Americas" data-toggle="tab">Du lịch Châu Mỹ</a></li>
                </ul>
            </div>
            <div class="tab-affiliate-marketing-bg">
                <div class="tab-content" id="tabs">
                    <div class="tab-pane active img_before" id="Asia">
                        <div class="container">
                            <div class="row row-onclick-ads">
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-malaysia-singapore-thuy-cung-s-e-a-aquarium" title="Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM]">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/kuala-lumpur.jpg?v=1529555728220" alt="Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM]">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM] ">
                                                Du lịch Malaysia - Singapore [Thủy cung S.E.A AQUARIUM]</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.579.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Di chuyển bằng tàu thủy">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1610202220100" alt="Di chuyển bằng tàu thủy">
                                                        </li>
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-thuyen-5-sao-voyager-of-the-seas" title="Du thuyền 5 Sao Voyager Of The Seas">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/south-east-asia-cruise-package-6-min-2.jpg?v=1529555857067" alt="Du thuyền 5 Sao Voyager Of The Seas">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Phú Quốc - Bãi Sao - Vinpearl Land ">
                                                Du thuyền 5 Sao Voyager Of The Seas</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">4.230.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Di chuyển bằng tàu thủy" aria-describedby="tooltip930173">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_2.svg?1610202220100" alt="Di chuyển bằng tàu thủy">
                                                        </li>
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-han-quoc-hoa-anh-dao" title="Du lịch Hàn Quốc - Hoa anh đào">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/han-quoc-4-mua-hoa.jpg?v=1529555941507" alt="Du lịch Hàn Quốc - Hoa anh đào">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                                                Du lịch Hàn Quốc - Hoa anh đào</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.300.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane img_before" id="Europe">
                        <div class="container">
                            <div class="row row-onclick-ads">

                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-chau-au-phap-thuy-sy-nui-jungfrau-y" title="Du lịch Châu Âu Pháp - Thụy Sỹ - Núi Jungfrau - Ý">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/grand-britain-europe-tour-5-min.jpg?v=1529553857067" alt="Du lịch Châu Âu Pháp - Thụy Sỹ - Núi Jungfrau - Ý">
                                            </a>


                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Hà Nội - Lào Cai - Sapa - Hạ Long "></a>
                                                Du lịch Châu Âu Pháp - Thụy Sỹ - Núi Jungfrau - Ý </a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">7.990.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-phap-bi-ha-lan-hoi-hoa-tulip-keukenhof" title="Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof]">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/0r2a5723.jpg?v=1529553943837" alt="Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof]">
                                            </a>
                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof] ">
                                                    Du lịch Pháp - Bỉ - Hà Lan [Hội Hoa Tulip Keukenhof]</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.300.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-y-rome-pisa-florence-venice-milan" title="Du lịch Ý [Rome - Pisa - Florence - Venice - Milan]">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/rome-travel-tips-1.jpg?v=1529555503057" alt="Du lịch Ý [Rome - Pisa - Florence - Venice - Milan]">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas] ">
                                         Du lịch Ý [Rome - Pisa - Florence - Venice - Milan]</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.300.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane img_before" id="Australia">
                        <div class="container">
                            <div class="row row-onclick-ads">
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-uc-melbourne-canberra-sydney-trai-nghiem-truc-thang-ngam-dao-p" title="Du lịch Úc Melbourne - Canberra - Sydney - Trải nghiệm trực thăng ngắm đảo Phillip">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/forum-theatre-1473730075.jpg?v=1529555239057" alt="Du lịch Úc Melbourne - Canberra - Sydney - Trải nghiệm trực thăng ngắm đảo Phillip">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch khám phá Cái Bè - Cần Thơ - Châu Đốc - Hà Tiên ">
                                                Du lịch Úc Melbourne - Canberra - Sydney - Trải nghiệm trực thăng ngắm đảo Phillip</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.579.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane img_before" id="Americas">
                        <div class="container">
                            <div class="row row-onclick-ads">
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-my-los-angeles-las-vegas-universal-studios-hollywood-2-dem-ks" title="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/83864b64404979-5ad0e1bdba9b2.jpg?v=1529553163000" alt="Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch khám phá Cái Bè - Cần Thơ - Châu Đốc - Hà Tiên ">
                                                Du lịch Mỹ [Los Angeles - Las Vegas - Universal Studios Hollywood] [2 đêm KS 5* Bellagio, Las Vegas]</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.579.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-canada-cuba-vancouver-victoria-la-habana-varadero" title="Du lịch Canada - Cuba [vancouver - victoria - la habana - varadero]">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/vancouver-1.jpg?v=1529553306293" alt="Du lịch Canada - Cuba [vancouver - victoria - la habana - varadero]">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Canada - Cuba [vancouver - victoria - la habana - varadero]">
                                                Du lịch Canada - Cuba [vancouver - victoria - la habana - varadero]</a>
                                            </h3>
                                            <div class="price-tag">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">4.230.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-sm-6 col-xs-6 ">
                                    <div class="product-box  box-services">
                                        <div class="product-thumbnail ">
                                            <a href="/du-lich-nam-phi-johannesburg-pretoria-soweto-cape-town" title="Du lịch Nam Phi [Johannesburg - Pretoria - Soweto - Cape Town]">
                                                <img src="//bizweb.dktcdn.net/thumb/large/100/299/077/products/helicopter-view-cape-town-e1521821219262.jpg?v=1529555389733" alt="Du lịch Nam Phi [Johannesburg - Pretoria - Soweto - Cape Town]">
                                            </a>

                                        </div>
                                        <div class="product-inf a-left  padding-10">
                                            <h3 class="color-black font-16">
                                                <a class="line-clamp " href="# " title="Du lịch Nam Phi [Johannesburg - Pretoria - Soweto - Cape Town] ">
                                                Du lịch Nam Phi [Johannesburg - Pretoria - Soweto - Cape Town]</a>
                                            </h3>
                                            <div class="price-tag ">
                                                <div class="box-prices ">
                                                    <div class="price-box clearfix ">
                                                        <div class="special-price f-left ">
                                                            <span class="price product-price ">3.300.000₫</span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="box-tag ">
                                                    <ul class="ct_course_list ">
                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng Ô tô ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_1.svg?1607937814596 " alt="Di chuyển bằng Ô tô ">
                                                        </li>

                                                        <li data-toggle="tooltip " data-placement="top " title=" " data-original-title="Di chuyển bằng máy bay ">
                                                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_3.svg?1607937814596 " alt="Di chuyển bằng máy bay ">
                                                        </li>

                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="box-date-tour ">
                                                <ul class="ct_course_list ">
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_4.svg?1607937814596 " alt="Thứ 2 - 7 hằng tuần "></div> Khởi hành: Thứ 2 - 7 hằng tuần
                                                    </li>
                                                    <li class="clearfix ">
                                                        <div class="ulimg "><img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/tag_icon_5.svg?1607937814596 " alt="6 ngày 5 đêm "></div> Thời gian: 6 ngày 5 đêm
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>