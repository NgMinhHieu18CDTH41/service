<div class="blog-content">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="tour-new ">
                    <h2>Cẩm Nang Du Lịch

                    </h2>
                    <div class="title-line ">
                        <div class="tl-1 "></div>
                        <div class="tl-2 "></div>
                        <div class="tl-3 "></div>
                    </div>
                    <p>Những địa điểm du lịch hot nhất dịp Tết ở Việt Nam. Tham khảo những điểm du lịch đặc sắc nhất từ Bắc tới Nam cùng với chúng tôi.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="new_hot_left">
        <div class="container blogrepon">
            <div class="row">
                <div class="news_owl col-lg-6 col-md-6 col-sm-6 col-xs-12 camn-nang-du-lich">
                    <div class="item_blog_big">
                        <div class="figure-big">
                            <div class="img_thumb_blogs">
                                <a href="/xieu-long-voi-nhung-canh-dep-nen-tho-o-chua-huong" class="big_img_h">
                                    <picture>
                                        <source media="(max-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/chua-huong.jpg?v=1520693664270">
                                        <source media="(min-width: 481px) and (max-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/chua-huong.jpg?v=1520693664270">
                                        <source media="(min-width: 768px) and (max-width: 1023px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/chua-huong.jpg?v=1520693664270">
                                        <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/chua-huong.jpg?v=1520693664270">
                                        <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/grande/100/299/077/articles/chua-huong.jpg?v=1520693664270">
                                        <img src="//bizweb.dktcdn.net/100/299/077/articles/chua-huong.jpg?v=1520693664270" title="Xiêu lòng với những cảnh đẹp nên thơ ở chùa Hương" alt="Xiêu lòng với những cảnh đẹp nên thơ ở chùa Hương" class="img-responsive center-block">
                                    </picture>
                                </a>
                            </div>
                            <div class="content_item_blogs">
                                <div class="blog_home_title margin-top-10 margin-bottom-10">
                                    <h3 class="news_home_content_short_info">
                                        <a href="/xieu-long-voi-nhung-canh-dep-nen-tho-o-chua-huong" title="Xiêu lòng với những cảnh đẹp nên thơ ở chùa Hương">Xiêu lòng với những cảnh đẹp nên thơ ở chùa Hương</a>
                                    </h3>
                                </div>
                                <div class="content-sum">
                                    Vậy ở chùa Hương có gì thú vị mà lại thu hút nhiều du khách trong lẫn ngoài nước đến như vậy, chúng ta hãy cùng tìm hiểu xem nhé. Chùa Hương hay tên gọi đầy đủ là chùa Hương Sơn, là một quần thể di tích thắng cảnh với rất nhiều ngôi chùa, đền, đình, bao
                                    quanh là non nước hùng...
                                </div>
                                <div class="content_day_blog margin-bottom-10">
                                    <i class="fas fa-clock"></i><span>Saturday,</span>
                                    <span class="news_home_content_short_time">
                                        10/03/2018
                                    </span>
                                    <span class="cmt_count_blog">
                                        <i class="fas fa-comments" aria-hidden="true"></i>(2) Bình luận
                                    </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="content-blog-index col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="xxx clearfix">
                        <div class="myblog" onclick="window.location.href='/trang-an-co-diem-den-dang-hot-o-ninh-binh';">
                            <div class="item_blog_big">
                                <div class="figure-big">
                                    <div class="image-blog-left img_thumb_blogs">

                                        <a href="/trang-an-co-diem-den-dang-hot-o-ninh-binh">
                                            <picture>
                                                <source media="(max-width: 375px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/trang-an-2-5-1.jpg?v=1606138224437">
                                                <source media="(min-width: 376px) and (max-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/trang-an-2-5-1.jpg?v=1606138224437">
                                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/trang-an-2-5-1.jpg?v=1606138224437">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/trang-an-2-5-1.jpg?v=1606138224437">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/trang-an-2-5-1.jpg?v=1606138224437">
                                                <img src="//bizweb.dktcdn.net/100/299/077/articles/trang-an-2-5-1.jpg?v=1606138224437" title="Tràng An cổ – điểm đến đang hot ở Ninh Bình" alt="Tràng An cổ – điểm đến đang hot ở Ninh Bình">
                                            </picture>
                                        </a>

                                    </div>
                                </div>
                            </div>
                            <div class="content-right-blog">
                                <div class="title_blog_home">
                                    <h3>
                                        <a href="/trang-an-co-diem-den-dang-hot-o-ninh-binh" title="Tràng An cổ – điểm đến đang hot ở Ninh Bình">Tràng An cổ – điểm đến đang hot ở Ninh Bình</a>
                                    </h3>
                                </div>
                                <div class="content-sum">

                                    Ở Tràng An có hai địa danh là Tràng An và Tràng An cổ. Trong đó, Tràng An, nơi thu hút hàng nghìn lượt...
                                </div>
                                <div class="content_day_blog"><i class="fas fa-clock"></i><span>Saturday,</span>
                                    <span class="news_home_content_short_time">
                                    10/03/2018
                                </span>
                                    <span class="cmt_count_blog">
                                    <i class="fas fa-comments" aria-hidden="true"></i>(1) Bình luận
                                </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="xxx clearfix">
                        <div class="myblog" onclick="window.location.href='/mua-hoa-phan-phu-hong-troi-bao-loc';">
                            <div class="item_blog_big">
                                <div class="figure-big">
                                    <div class="image-blog-left img_thumb_blogs">

                                        <a href="/mua-hoa-phan-phu-hong-troi-bao-loc">
                                            <picture>
                                                <source media="(max-width: 375px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/7mai-anh-dao-dalat-zing.jpg?v=1520693432973">
                                                <source media="(min-width: 376px) and (max-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/7mai-anh-dao-dalat-zing.jpg?v=1520693432973">
                                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/7mai-anh-dao-dalat-zing.jpg?v=1520693432973">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/7mai-anh-dao-dalat-zing.jpg?v=1520693432973">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/7mai-anh-dao-dalat-zing.jpg?v=1520693432973">
                                                <img src="//bizweb.dktcdn.net/100/299/077/articles/7mai-anh-dao-dalat-zing.jpg?v=1520693432973" title="Mùa hoa phấn phủ hồng trời Bảo Lộc" alt="Mùa hoa phấn phủ hồng trời Bảo Lộc">
                                            </picture>
                                        </a>

                                    </div>
                                </div>
                            </div>
                            <div class="content-right-blog">
                                <div class="title_blog_home">
                                    <h3>
                                        <a href="/mua-hoa-phan-phu-hong-troi-bao-loc" title="Mùa hoa phấn phủ hồng trời Bảo Lộc">Mùa hoa phấn phủ hồng trời Bảo Lộc</a>
                                    </h3>
                                </div>
                                <div class="content-sum">

                                    Hoa phấn hồng còn được nhiều người gọi là hoa kèn hồng. Đây là loại cây thân gỗ, chiều cao trung bình...
                                </div>
                                <div class="content_day_blog"><i class="fas fa-clock"></i><span>Saturday,</span>
                                    <span class="news_home_content_short_time">
                                    10/03/2018
                                </span>
                                    <span class="cmt_count_blog">
                                    <i class="fas fa-comments" aria-hidden="true"></i>(0) Bình luận
                                </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="xxx clearfix">
                        <div class="myblog" onclick="window.location.href='/ai-bao-da-lat-chi-hop-style-mo-mong-cool-ngau-nhu-doi-ban-than-nay-van-co-ca-ro';">
                            <div class="item_blog_big">
                                <div class="figure-big">
                                    <div class="image-blog-left img_thumb_blogs">

                                        <a href="/ai-bao-da-lat-chi-hop-style-mo-mong-cool-ngau-nhu-doi-ban-than-nay-van-co-ca-ro">
                                            <picture>
                                                <source media="(max-width: 375px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/dalat-1.jpg?v=1520693176427">
                                                <source media="(min-width: 376px) and (max-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/dalat-1.jpg?v=1520693176427">
                                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/dalat-1.jpg?v=1520693176427">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/dalat-1.jpg?v=1520693176427">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/dalat-1.jpg?v=1520693176427">
                                                <img src="//bizweb.dktcdn.net/100/299/077/articles/dalat-1.jpg?v=1520693176427" title="Ai bảo Đà Lạt chỉ hợp style mơ mộng? Cool ngầu như đôi bạn thân này vẫn có cả rổ ảnh thần thái!" alt="Ai bảo Đà Lạt chỉ hợp style mơ mộng? Cool ngầu như đôi bạn thân này vẫn có cả rổ ảnh thần thái!">
                                            </picture>
                                        </a>

                                    </div>
                                </div>
                            </div>
                            <div class="content-right-blog">
                                <div class="title_blog_home">
                                    <h3>
                                        <a href="/ai-bao-da-lat-chi-hop-style-mo-mong-cool-ngau-nhu-doi-ban-than-nay-van-co-ca-ro" title="Ai bảo Đà Lạt chỉ hợp style mơ mộng? Cool ngầu như đôi bạn thân này vẫn có cả rổ ảnh thần thái!">Ai bảo Đà Lạt chỉ hợp style mơ mộng? Cool ngầu như đôi bạn thân này vẫn có cả rổ ảnh thần thái!</a>
                                    </h3>
                                </div>
                                <div class="content-sum">
                                    Ai bảo Đà Lạt chỉ hợp style mơ mộng? Cool ngầu như đôi bạn thân này vẫn có cả rổ ảnh thần thái! Mỗi khi...
                                </div>
                                <div class="content_day_blog"><i class="fas fa-clock"></i><span>Saturday,</span>
                                    <span class="news_home_content_short_time">
                                    10/03/2018
                                </span>
                                    <span class="cmt_count_blog">
                                    <i class="fas fa-comments" aria-hidden="true"></i>(1) Bình luận
                                </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="xxx clearfix">
                        <div class="myblog" onclick="window.location.href='/net-binh-di-viet-nam-qua-anh-cua-tay-may-ha-lan';">
                            <div class="item_blog_big">
                                <div class="figure-big">
                                    <div class="image-blog-left img_thumb_blogs">

                                        <a href="/net-binh-di-viet-nam-qua-anh-cua-tay-may-ha-lan">
                                            <picture>
                                                <source media="(max-width: 375px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/articles/du-lich-hoi-an-11.jpg?v=1520693088693">
                                                <source media="(min-width: 376px) and (max-width: 767px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/du-lich-hoi-an-11.jpg?v=1520693088693">
                                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/du-lich-hoi-an-11.jpg?v=1520693088693">
                                                <source media="(min-width: 768px) and (max-width: 1023px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/du-lich-hoi-an-11.jpg?v=1520693088693">
                                                <source media="(min-width: 1024px) and (max-width: 1199px)" srcset="//bizweb.dktcdn.net/thumb/compact/100/299/077/articles/du-lich-hoi-an-11.jpg?v=1520693088693">
                                                <img src="//bizweb.dktcdn.net/100/299/077/articles/du-lich-hoi-an-11.jpg?v=1520693088693" title="Nét bình dị Việt Nam qua ảnh của tay máy Hà Lan" alt="Nét bình dị Việt Nam qua ảnh của tay máy Hà Lan">
                                            </picture>
                                        </a>

                                    </div>
                                </div>
                            </div>
                            <div class="content-right-blog">
                                <div class="title_blog_home">
                                    <h3>
                                        <a href="/net-binh-di-viet-nam-qua-anh-cua-tay-may-ha-lan" title="Nét bình dị Việt Nam qua ảnh của tay máy Hà Lan">Nét bình dị Việt Nam qua ảnh của tay máy Hà Lan</a>
                                    </h3>
                                </div>
                                <div class="content-sum">
                                    Những hình ảnh này được giới thiệu trong mục Du lịch Instagram snapshots ngày 28-2 của báo Anh Guardian. ...
                                </div>
                                <div class="content_day_blog"><i class="fas fa-clock"></i><span>Saturday,</span>
                                    <span class="news_home_content_short_time">
                                    10/03/2018
                                </span>
                                    <span class="cmt_count_blog">
                                    <i class="fas fa-comments" aria-hidden="true"></i>(0) Bình luận
                                </span>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>