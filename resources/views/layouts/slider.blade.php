{{--<h1 class="hidden">Ant Du lịch - Ant Du lịch - Chúng tôi cam kết luôn nỗ lực đem đến những giá trị dịch vụ tốt nhất cho khách hàng và đối tác để tiếp tục khẳng định vị trí hàng đầu của thương hiệu Ant Du lịch.</h1>--}}
<section class="awe-section-1">
    <div class="home-slider owl-carousel not-dqowl owl-loaded owl-drag">
        <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-3072px, 0px, 0px); transition: all 0s ease 0s; width: 7680px;"><div class="owl-item cloned" style="width: 1536px;"><div class="item">
                        <a href="#" class="clearfix">
                            <picture>
                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <img src="//bizweb.dktcdn.net/thumb/grande/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100" alt="New Collection" class="img-responsive center-block">
                            </picture>
                        </a>
                    </div></div><div class="owl-item cloned" style="width: 1536px;"><div class="item">
                        <a href="#" class="clearfix">
                            <picture>
                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <img src="//bizweb.dktcdn.net/thumb/grande/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100" alt="New Collection" class="img-responsive center-block">
                            </picture>
                        </a>
                    </div></div><div class="owl-item active" style="width: 1536px;"><div class="item">
                        <a href="#" class="clearfix">
                            <picture>
                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <img src="//bizweb.dktcdn.net/thumb/grande/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100" alt="New Collection" class="img-responsive center-block">
                            </picture>
                        </a>
                    </div></div><div class="owl-item cloned" style="width: 1536px;"><div class="item">
                        <a href="#" class="clearfix">
                            <picture>
                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <img src="//bizweb.dktcdn.net/thumb/grande/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100" alt="New Collection" class="img-responsive center-block">
                            </picture>
                        </a>
                    </div></div><div class="owl-item cloned" style="width: 1536px;"><div class="item">
                        <a href="#" class="clearfix">
                            <picture>
                                <source media="(min-width: 1200px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 992px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 569px)" srcset="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <source media="(min-width: 480px)" srcset="//bizweb.dktcdn.net/thumb/large/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100">
                                <img src="//bizweb.dktcdn.net/thumb/grande/100/299/077/themes/642224/assets/slider_1.jpg?1610202220100" alt="New Collection" class="img-responsive center-block">
                            </picture>
                        </a>
                    </div></div></div></div><div class="owl-nav disabled"><div class="owl-prev">prev</div><div class="owl-next">next</div></div><div class="owl-dots disabled"><div class="owl-dot active"><span></span></div></div></div>


</section>
