
<div id="header">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="top-info">
                        <li>
                            <i class="fas fa-phone"></i>
                            <a href="">0999 999 999</a>
                        </li>
                        <li>
                            <i class="fas fa-envelope"></i>
                            <a href="">abc@contact.com</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="singin f-right">
                        <li>
                            <a href="{{URL::to('/login')}}">
                                <i class="fas fa-sign-out-alt"></i> Đăng Nhập
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="fas fa-user-plus"></i> Đăng Kí
                            </a>
                        </li>
                    </ul>
                </div>
                <br>
            </div>
        </div>
    </div>
    <div class="header-main">
        <div class="container">
            <div class="row">
                <div class="col-md-3 logorepon">
                    <div id="mySidebar" class="sidebar">
                        <div class="c-menu--slide-left d-md-none " id="menu-mobile-button" style="transition: 1s;">
                            <div class="la-nav-top-login ">
                                <div class="la-avatar-nav p-relative text-center ">
                                    <a href="/account ">
                                        <img class="img-responsive " src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/av-none-user.png?1607937814596 " alt="avatar ">
                                    </a>
                                    <div class="la-hello-user-nav ng-scope ">Xin chào</div>
                                    <img id="close-nav" onclick="closeNav()" class="c-menu__close" src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/ic-close-menu.png?1607937814596 " alt="closenav ">
                                </div>
                                <div class="la-action-link-nav text-center ">
                                    <a href="/account/login " class="uppercase ">ĐĂNG NHẬP</a>
                                    <a href="/account/register " class="uppercase ">ĐĂNG KÝ</a>

                                </div>
                            </div>
                            <div class="la-scroll-fix-infor-user ">
                                <!--CATEGORY-->
                                <div class="la-nav-menu-items ">
                                    <div class="la-title-nav-items">Tất cả danh mục</div>
                                    <ul class="la-nav-list-items">
                                        <li class="ng-scope">
                                            <a href="/">Trang chủ</a>
                                        </li>
                                        <li class="ng-scope ">
                                            <a href="/gioi-thieu ">Giới thiệu</a>
                                        </li>
                                        <li class="ng-scope ng-has-child1 ">
                                            <a href="/tour-trong-nuoc ">Tour trong nước <i  id="oknhe" class="fas fa-plus fa1" aria-hidden="true "></i></a>
                                            <ul class="ul-has-child1 ">
                                                <li class="ng-scope ng-has-child2 ">
                                                    <a id="oknhe1" href="">Miền Trung <i class="fas fa-plus fa2 " aria-hidden="true "></i></a>
                                                    <ul class="ul-has-child2 ">
                                                        <li class="ng-scope">
                                                            <a href="/du-lich-quang-binh ">Du lịch Quảng Bình</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-hue ">Du lịch Huế</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-da-nang ">Du lịch Đà Nẵng</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-hoi-an ">Du lịch Hội An</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-nha-trang ">Du lịch Nha Trang</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-phan-thiet ">Du lịch Phan Thiết</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-da-lat ">Du lịch Đà Lạt</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="ng-scope ng-has-child2 ">
                                                    <a href="/mien-bac ">Miền Bắc <i class="fas fa-plus fa2" aria-hidden="true "></i></a>
                                                    <ul class="ul-has-child2 ">
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-ha-noi ">Du lịch Hà Nội</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-ha-long ">Du lịch Hạ Long</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-sapa ">Du lịch Sapa</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-ninh-binh ">Du lịch Ninh Bình</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-hai-phong ">Du lịch Hải Phòng</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="ng-scope ng-has-child2 ">
                                                    <a href="/mien-nam ">Miền Nam <i class="fas fa-plus fa2 " aria-hidden="true "></i></a>
                                                    <ul class="ul-has-child2 ">
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-phu-quoc ">Du lịch Phú Quốc</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-con-dao ">Du lịch Côn Đảo</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-can-tho ">Du lịch Cần Thơ</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-vung-tau ">Du lịch Vũng Tàu</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-ben-tre ">Du lịch Bến Tre</a>
                                                        </li>
                                                        <li class="ng-scope ">
                                                            <a href="/du-lich-dao-nam-du ">Du lịch Đảo Nam Du</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="ng-scope ng-has-child1 ">
                                            <a href="/tour-nuoc-ngoai ">Tour nước ngoài <i class="fas fa-plus fa1 " aria-hidden="true "></i></a>
                                            <ul class="ul-has-child1 ">
                                                <li class="ng-scope ">
                                                    <a href="/du-lich-chau-a ">Du lịch Châu Á</a>
                                                </li>
                                                <li class="ng-scope ">
                                                    <a href="/du-lich-chau-au ">Du lịch Châu Âu</a>
                                                </li>
                                                <li class="ng-scope ">
                                                    <a href="/du-lich-chau-uc ">Du lịch Châu Úc</a>
                                                </li>
                                                <li class="ng-scope ">
                                                    <a href="/du-lich-chau-my ">Du lịch Châu Mỹ</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="ng-scope ">
                                            <a href="/dich-vu-tour ">Dịch vụ tour</a>
                                        </li>
                                        <li class="ng-scope ">
                                            <a href="/cam-nang-du-lich ">Cẩm nang du lịch</a>
                                        </li>
                                        <li class="ng-scope ">
                                            <a href="/lien-he ">Liên hệ</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="la-nav-slide-banner ">

                                    <a href="# ">
                                        <img alt="Ant Du lịch " src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/left-menu-banner-1.png?1607937814596 ">
                                    </a>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="menu-mobile-button">
                        <button id="openbtn" type="button" class="nav-line-group d-md-none hidden-lg hidden-md" onclick="openNav()">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar "></span>
                                <span class="icon-bar "></span>
                                <span class="icon-bar "></span>
                            </button>

                    </div>
                    <div class="logo">
                        <a href="{{route('trang_chu')}}" class="logo-wrapper ">
                            <img src="https://bizweb.dktcdn.net/100/299/077/themes/642224/assets/logo.png?1607937814596 " alt=" "></a>
                    </div>
                </div>
                <div class="col-md-5 search1">
                    <div class="search">
                        <div class="header_search search_form">
                            <form class="input-group search-bar search_form has-validation-callback" action="/search" method="get" role="search">
                                <input type="search" name="query" value="" placeholder="Tìm kiếm tour..." class="input-group-field st-default-search-input search-text" autocomplete="off">
                                <span class="input-group-btn">
                                        <button class="btn icon-fallback-text">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </span>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden-sm hidden-xs ">
                    <div class="top-fun ">
                        <div class="hotline ">
                            <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/hotline.svg?1607937814596 " alt="Tổng đài miễn phí ">
                            <div class="hotline-text ">

                                <a href="tel:0982362509 ">0982 362 509</a>

                                <span>Tổng đài miễn phí</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu" id="menu1">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12 ">
                    <div class="navbar ">
                        <a href="#home ">Trang chủ</a>
                        <a href="#home ">Giới thiệu</a>
                        @php($categories = \Modules\Category\Entities\Category::with('model_machines')->orderByDesc('id')->get())

                            @foreach($categories as $ct)
                        <div class="subnav ">
                            <button class="subnavbtn ">{{$ct->name}}<i class="fas fa-angle-right "></i></button>
                            <div class="subnav-content ">
                                <ul class="level0 ">
                                    @foreach($ct->model_machines as $mm)
                                    <li class="level1 parent item">
                                        <h2 class="h4">
                                            <a href=""><span>{{$mm -> name}}</span></a>
                                        </h2>
                                        <ul class="level2">
                                            @forelse($mm->products as $p)
                                            <li class="level2"> <a href=" "><span>{{$p->name}}</span></a></li>
                                            @empty
                                            @endforelse
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        @endforeach
{{--                        <div class="subnav-tour">--}}
{{--                            <button class="subnavbtn">Tour nước ngoài<i class="fas fa-angle-right "></i></button>--}}
{{--                            <div class="tour-new1">--}}
{{--                                <ul class="dropdown-menu">--}}
{{--                                    <li class="nav-item-level2"><a class="nav-link" href=" \">Miền Trung</a></li>--}}
{{--                                    <li class="nav-item-level2"><a class="nav-link" href=" ">Miền Trung</a></li>--}}
{{--                                    <li class="nav-item-level2"><a class="nav-link" href=" ">Miền Trung</a></li>--}}
{{--                                    <li class="nav-item-level2"> <a class="nav-link" href=" ">Miền Trung</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <a href="#home ">Du lịch tour</a>
                        <a href="#home ">Cẩm nang du lịch</a>
                        <a href="#home ">Liên hệ</a>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
