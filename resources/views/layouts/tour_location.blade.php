<div class="section_tour-location">
    <div class="container ">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="tour-new ">
                    <h2>Điểm Đến
                        <span>Ưa Thích</span>
                    </h2>
                    <div class="title-line ">
                        <div class="tl-1 "></div>
                        <div class="tl-2 "></div>
                        <div class="tl-3 "></div>
                    </div>
                    <p>Cẩm nang thông tin về du lịch, văn hóa, ẩm thực, các sự kiện và lễ hội tại các điểm đến Việt nam, Đông Nam Á và Thế Giới.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid d-block">
        <div class="row">
            <div class="section-location-owl owl-carousel owl-theme not-dqowl owl-loaded owl-drag">
                <div class="owl-stage-outer">
                    <div class="owl-stage">
                        <div class="owl-item active">
                            <div class="item">
                                <div class="tourmaster-tour-category-item-wrap">
                                    <div class="tourmaster-tour-category-thumbnail tourmaster-media-image">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/location_image_1.png?1610202220100" alt="Phú Quốc">
                                    </div>
                                    <div class="tourmaster-tour-category-overlay"></div>
                                    <div class="tourmaster-tour-category-overlay-front"></div>
                                    <div class="tourmaster-tour-category-head">
                                        <div class="tourmaster-tour-category-head-display clearfix">
                                            <h3 class="tourmaster-tour-category-title"><i class="fas fa-map-marker-alt color-x"></i>Phú Quốc</h3>
                                        </div>
                                        <div class="tourmaster-tour-category-head-animate">
                                            <a class="tourmaster-tour-category-head-link" href="/collections/all">Xem chi tiết</a>
                                            <div class="tourmaster-tour-category-head-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="item">
                                <div class="tourmaster-tour-category-item-wrap">
                                    <div class="tourmaster-tour-category-thumbnail tourmaster-media-image">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/location_image_2.png?1610202220100" alt="Châu Á">
                                    </div>
                                    <div class="tourmaster-tour-category-overlay"></div>
                                    <div class="tourmaster-tour-category-overlay-front"></div>
                                    <div class="tourmaster-tour-category-head">
                                        <div class="tourmaster-tour-category-head-display clearfix">
                                            <h3 class="tourmaster-tour-category-title"><i class="fas fa-map-marker-alt color-x"></i>Châu Á</h3>
                                        </div>
                                        <div class="tourmaster-tour-category-head-animate">
                                            <a class="tourmaster-tour-category-head-link" href="/collections/all">Xem chi tiết</a>
                                            <div class="tourmaster-tour-category-head-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="item">
                                <div class="tourmaster-tour-category-item-wrap">
                                    <div class="tourmaster-tour-category-thumbnail tourmaster-media-image">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/location_image_3.png?1610202220100" alt="Châu Âu">
                                    </div>
                                    <div class="tourmaster-tour-category-overlay"></div>
                                    <div class="tourmaster-tour-category-overlay-front"></div>
                                    <div class="tourmaster-tour-category-head">
                                        <div class="tourmaster-tour-category-head-display clearfix">
                                            <h3 class="tourmaster-tour-category-title"><i class="fas fa-map-marker-alt color-x"></i>Châu Âu</h3>
                                        </div>
                                        <div class="tourmaster-tour-category-head-animate">
                                            <a class="tourmaster-tour-category-head-link" href="/collections/all">Xem chi tiết</a>
                                            <div class="tourmaster-tour-category-head-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="item">
                                <div class="tourmaster-tour-category-item-wrap">
                                    <div class="tourmaster-tour-category-thumbnail tourmaster-media-image">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/location_image_4.png?1610202220100" alt="Châu Mỹ">
                                    </div>
                                    <div class="tourmaster-tour-category-overlay"></div>
                                    <div class="tourmaster-tour-category-overlay-front"></div>
                                    <div class="tourmaster-tour-category-head">
                                        <div class="tourmaster-tour-category-head-display clearfix">
                                            <h3 class="tourmaster-tour-category-title"><i class="fas fa-map-marker-alt color-x"></i>Châu Mỹ</h3>
                                        </div>
                                        <div class="tourmaster-tour-category-head-animate">
                                            <a class="tourmaster-tour-category-head-link" href="/collections/all">Xem chi tiết</a>
                                            <div class="tourmaster-tour-category-head-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="owl-item active">
                            <div class="item">
                                <div class="tourmaster-tour-category-item-wrap">
                                    <div class="tourmaster-tour-category-thumbnail tourmaster-media-image">
                                        <img src="//bizweb.dktcdn.net/100/299/077/themes/642224/assets/location_image_5.png?1610202220100" alt="Châu Phi">
                                    </div>
                                    <div class="tourmaster-tour-category-overlay"></div>
                                    <div class="tourmaster-tour-category-overlay-front"></div>
                                    <div class="tourmaster-tour-category-head">
                                        <div class="tourmaster-tour-category-head-display clearfix">
                                            <h3 class="tourmaster-tour-category-title"><i class="fas fa-map-marker-alt color-x"></i>Châu Phi</h3>
                                        </div>
                                        <div class="tourmaster-tour-category-head-animate">
                                            <a class="tourmaster-tour-category-head-link" href="/collections/all">Xem chi tiết</a>
                                            <div class="tourmaster-tour-category-head-divider"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>