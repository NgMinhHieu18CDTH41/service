<?php

namespace App\Http\Controllers;

use Modules\Category\Entities\Category;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(){
        $categories = Category::with('model_machines')->orderByDesc('id')->get();
        return view('tour-details',compact('categories'));
    }
}
